# Dead Cats Bounce

A long time ago, in a basement far, far, away, I attempted to predict stock prices one day into the future, based on the idea that ["even dead cats bounce"](https://en.wikipedia.org/wiki/Dead_cat_bounce) (extreme events have predictable regression toward mean). 


Circa 2007, it *was* showing reasonable success (according to simulations), however when I restarted the simulations in 2013 it was an abismal failure. As of 2020, I don't know if markets have changed, or I misprogrammed the simulator.

Don't judge me, it was a long time ago. I share this as an amusement.

## Contents

The collections consists of exported scripts from MS SQL Server as well as some postgres ports.
