USE [port]
GO
/****** Object:  Table [dbo].[tAuthUsers]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tAuthUsers](
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_tAuthUsers] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tFinCurrencies]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tFinCurrencies](
	[CurrID] [int] NOT NULL,
	[Code] [char](3) NOT NULL,
	[Num] [smallint] NULL,
	[Name] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tFinDaily]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tFinDaily](
	[ExchID] [tinyint] NOT NULL,
	[SecID] [int] NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[valOpen] [money] NULL,
	[valClose] [money] NULL,
	[valHigh] [money] NULL,
	[valLow] [money] NULL,
	[Change] [money] NULL,
	[Volume] [bigint] NULL,
	[Trades] [int] NULL,
	[CADClose] [money] NULL,
	[BaseClose] [money] NULL,
 CONSTRAINT [PK_tFinDaily] PRIMARY KEY CLUSTERED 
(
	[ExchID] ASC,
	[SecID] ASC,
	[Date] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tFinExchanges]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tFinExchanges](
	[ExchID] [tinyint] NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Symb] [varchar](10) NULL,
	[Curr] [int] NOT NULL,
 CONSTRAINT [PK_tFinExchanges] PRIMARY KEY CLUSTERED 
(
	[ExchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tFinOrganizations]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tFinOrganizations](
	[OrgID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](60) NOT NULL,
 CONSTRAINT [PK_tFinOrganizations] PRIMARY KEY CLUSTERED 
(
	[OrgID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tFinSecSymb]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tFinSecSymb](
	[ExchID] [tinyint] NOT NULL,
	[SecID] [int] NOT NULL,
	[Symb] [varchar](10) NOT NULL,
	[Expired] [smalldatetime] NULL,
	[ReplaceWith] [varchar](10) NULL,
 CONSTRAINT [PK_tFinSecSymb] PRIMARY KEY CLUSTERED 
(
	[ExchID] ASC,
	[SecID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tFinSecurities]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tFinSecurities](
	[SecID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[OrgId] [int] NULL,
 CONSTRAINT [PK_tFinSecurities] PRIMARY KEY CLUSTERED 
(
	[SecID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPortHistory]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPortHistory](
	[PortID] [int] NOT NULL,
	[MarkTime] [datetime] NOT NULL,
	[NAV] [money] NOT NULL,
	[Units] [money] NULL,
	[UnitVal] [money] NULL,
 CONSTRAINT [PK_tPortHistory] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC,
	[MarkTime] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPortPortfolios]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tPortPortfolios](
	[PortID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Owner] [int] NOT NULL,
	[DefUnitVal] [money] NOT NULL CONSTRAINT [DF_tPortPortfolios_DefUnitVal]  DEFAULT (100),
 CONSTRAINT [PK_tPortPortfolios] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit value to be used if the portfolio value is $0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tPortPortfolios', @level2type=N'COLUMN',@level2name=N'DefUnitVal'
GO
/****** Object:  Table [dbo].[tPortPos]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPortPos](
	[PortID] [int] NOT NULL,
	[ExchID] [smallint] NOT NULL,
	[SecID] [int] NOT NULL,
	[oDate] [datetime] NOT NULL,
	[cDate] [datetime] NULL,
	[NetPos] [money] NOT NULL,
	[NetCost] [money] NOT NULL,
 CONSTRAINT [PK_tPortPos] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC,
	[ExchID] ASC,
	[SecID] ASC,
	[oDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPortProperties]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tPortProperties](
	[name] [varchar](20) NOT NULL,
	[value] [varchar](20) NOT NULL,
 CONSTRAINT [PK_tPortProperties] PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPortTradeItems]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPortTradeItems](
	[PortID] [int] NOT NULL,
	[TradeID] [int] NOT NULL,
	[ExchID] [int] NOT NULL,
	[SecID] [int] NOT NULL,
	[units] [money] NOT NULL,
	[price] [money] NOT NULL,
	[expenses] [money] NULL CONSTRAINT [DF_tPortTradeItems_expenses]  DEFAULT (0),
	[totcost] [money] NOT NULL,
 CONSTRAINT [PK_tPortTradeItems] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC,
	[TradeID] ASC,
	[ExchID] ASC,
	[SecID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPortTradeNotes]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tPortTradeNotes](
	[PortID] [int] NOT NULL,
	[TransID] [int] NOT NULL,
	[NoteOrd] [tinyint] NOT NULL,
	[Note] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tPortTradeNote] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC,
	[TransID] ASC,
	[NoteOrd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPortTrades]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPortTrades](
	[PortID] [int] NOT NULL,
	[TradeID] [int] NOT NULL,
	[Time] [datetime] NOT NULL,
	[Amt] [money] NULL,
 CONSTRAINT [PK_tPortTrades] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC,
	[TradeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPortUserPorts]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPortUserPorts](
	[UserID] [int] NOT NULL,
	[PortID] [int] NOT NULL,
	[isManager] [bit] NULL CONSTRAINT [DF_tPortUserPorts_isManager]  DEFAULT (0),
 CONSTRAINT [PK_tPortUserPorts] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[PortID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPortUserTrans]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPortUserTrans](
	[UserID] [int] NOT NULL,
	[PortID] [int] NOT NULL,
	[transdate] [datetime] NOT NULL,
	[units] [money] NOT NULL,
	[price] [money] NOT NULL,
	[total] [money] NULL,
 CONSTRAINT [PK_tPortUserAcct] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[PortID] ASC,
	[transdate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ttmp]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ttmp](
	[date] [smalldatetime] NOT NULL,
	[Value] [money] NULL,
	[Units] [money] NULL,
	[UnitVal] [money] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tTmpBaseClose]    Script Date: 09/20/2007 20:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tTmpBaseClose](
	[exchid] [tinyint] NOT NULL,
	[secid] [int] NOT NULL,
	[date] [smalldatetime] NOT NULL,
	[baseclose] [money] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tFinDaily]  WITH CHECK ADD  CONSTRAINT [FK_tFinDaily_tFinSecSymb] FOREIGN KEY([ExchID], [SecID])
REFERENCES [dbo].[tFinSecSymb] ([ExchID], [SecID])
GO
ALTER TABLE [dbo].[tFinDaily] CHECK CONSTRAINT [FK_tFinDaily_tFinSecSymb]
GO
ALTER TABLE [dbo].[tFinSecSymb]  WITH NOCHECK ADD  CONSTRAINT [FK_tFinSecSymb_tFinExchanges] FOREIGN KEY([ExchID])
REFERENCES [dbo].[tFinExchanges] ([ExchID])
GO
ALTER TABLE [dbo].[tFinSecSymb] CHECK CONSTRAINT [FK_tFinSecSymb_tFinExchanges]
GO
ALTER TABLE [dbo].[tFinSecSymb]  WITH CHECK ADD  CONSTRAINT [FK_tFinSecSymb_tFinSecurities] FOREIGN KEY([SecID])
REFERENCES [dbo].[tFinSecurities] ([SecID])
GO
ALTER TABLE [dbo].[tFinSecSymb] CHECK CONSTRAINT [FK_tFinSecSymb_tFinSecurities]
GO
ALTER TABLE [dbo].[tFinSecurities]  WITH NOCHECK ADD  CONSTRAINT [FK_tFinSecurities_tFinOrganizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[tFinOrganizations] ([OrgID])
GO
ALTER TABLE [dbo].[tFinSecurities] CHECK CONSTRAINT [FK_tFinSecurities_tFinOrganizations]
GO
ALTER TABLE [dbo].[tPortHistory]  WITH CHECK ADD  CONSTRAINT [FK_tPortHistory_tPortPortfolios] FOREIGN KEY([PortID])
REFERENCES [dbo].[tPortPortfolios] ([PortID])
GO
ALTER TABLE [dbo].[tPortHistory] CHECK CONSTRAINT [FK_tPortHistory_tPortPortfolios]
GO
ALTER TABLE [dbo].[tPortTradeItems]  WITH CHECK ADD  CONSTRAINT [FK_tPortTradeItems_tFinSecurities] FOREIGN KEY([SecID])
REFERENCES [dbo].[tFinSecurities] ([SecID])
GO
ALTER TABLE [dbo].[tPortTradeItems] CHECK CONSTRAINT [FK_tPortTradeItems_tFinSecurities]
GO
ALTER TABLE [dbo].[tPortTradeItems]  WITH CHECK ADD  CONSTRAINT [FK_tPortTradeItems_tPortTrades] FOREIGN KEY([PortID], [TradeID])
REFERENCES [dbo].[tPortTrades] ([PortID], [TradeID])
GO
ALTER TABLE [dbo].[tPortTradeItems] CHECK CONSTRAINT [FK_tPortTradeItems_tPortTrades]
GO
ALTER TABLE [dbo].[tPortTrades]  WITH NOCHECK ADD  CONSTRAINT [FK_tPortTrades_tPortPortfolios] FOREIGN KEY([PortID])
REFERENCES [dbo].[tPortPortfolios] ([PortID])
GO
ALTER TABLE [dbo].[tPortTrades] CHECK CONSTRAINT [FK_tPortTrades_tPortPortfolios]