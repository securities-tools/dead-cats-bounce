USE [port]
GO
/****** Object:  UserDefinedFunction [dbo].[fPortUnits]    Script Date: 09/20/2007 20:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[fPortUnits](@PORTID int,@TIME datetime=NULL)
returns money
as begin
  declare @value money
  
  if(@TIME is Null)begin
    set @TIME = '2069-01-01'
  end
  
  select @value = sum(units)
  from   tPortUserTrans
  where  portid = @Portid and 
         transdate <= @TIME 
  
  return @value
  
end

GO
/****** Object:  UserDefinedFunction [dbo].[fPortVal]    Script Date: 09/20/2007 20:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE   function [dbo].[fPortVal](@PORTID int, @TIME datetime)
returns money
as begin
  
  declare @value money
  declare @tmp money
  
  if(@TIME is Null)begin
    set @TIME = '2069-01-01'
  end
  
  declare curTots cursor for
  select sum(i.units)* isnull(val.cadclose,val.valclose) as val
  from   tPortTrades t
         inner join tPortTradeItems i on t.portid = i.portid and
                                         t.tradeid = i.tradeid
         inner join tFinDaily val     on val.exchid = i.exchid and
                                         val.secid = i.secid and
                                         val.date = (select max(date) from tFinDaily where exchid = i.exchid and secid = i.secid and date <= @TIME)
  where  t.portid = @PORTID and
         t.time <= @TIME
  group by t.portid,
           i.exchid,
           i.secid,
           val.cadclose,
           val.valclose
  
  set @value = 0
  open curTots
  fetch next from curTots into @tmp
  while @@FETCH_STATUS=0 begin
    set @value = @value + @tmp
    fetch next from curTots into @tmp
  end
  close curTots
  deallocate curTots
  
  return @value
  
end
