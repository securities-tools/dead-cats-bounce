USE [port]
GO
/****** Object:  StoredProcedure [dbo].[sImport]    Script Date: 09/20/2007 20:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*** sImportDaily **************************************************************
*
*  
*******************************************************************************/
CREATE PROCEDURE [dbo].[sImport]
AS BEGIN
  select s.secid,
         e.localexchid,
         d.date,
         d.valOpen,
         d.valClose,
         d.valHigh,
         d.valLow,
         d.Change,
         d.Volume,
         d.Trades
  into   #tTmpPortUpdate
  from   port.dbo.tFinSecSymb s 
         inner join stockhistory.dbo.tImportExch e on s.exchid = e.localexchid
         inner join stockhistory.dbo.tRawDaily d on d.exch = e.exchid and 
                                   d.security = s.symb and
                                   --ignore cases where nothing has changed
                                   not (d.change = 0 and d.trades = 0 and d.valClose = d.valOpen and d.valClose = d.valHigh and d.valClose = d.valLow)
         left join  port.dbo.tFinDaily fd on fd.exchid = e.localexchid and
                                             fd.secid = s.secid and
                                             fd.date = d.date
  where  s.expired is null and 
         fd.secid is null
  
  insert into port.dbo.tFinDaily(SecID,ExchID,Date,valOpen,valClose,valHigh,valLow,Change,Volume,Trades) --10 rows
  select * from #tTmpPortUpdate
  
  delete from stockhistory.dbo.tImportError 
  
  insert into stockhistory.dbo.tImportError
  select d.*
  from   stockhistory.dbo.tRawDaily                      d
         inner join stockhistory.dbo.tImportExch         i on d.exch = i.exchid
         left join port.dbo.tFinSecSymb s on s.symb = d.security and
                                             s.exchid = i.localexchid and
                                             s.expired is null
  where  s.symb is null


  -- convert values to canadian
  update port.dbo.tfindaily
  set    cadclose = valclose * ( select r.cadclose 
                                 from   port.dbo.tfinexchanges e
                                        inner join port.dbo.tfindaily r on r.exchid = 0 and
                                                                           r.secid = e.curr and
                                                                           r.date = (select max(a.date) from port.dbo.tfindaily a where a.exchid = 0 and a.secid = e.curr)
                                 where  e.exchid = tfindaily.exchid
                               )
  where  cadclose is null

  -- update portfolio NAVs
  exec sUpdatePortHistory
  
END


GO
/****** Object:  StoredProcedure [dbo].[sTraderPrecMetals]    Script Date: 09/20/2007 20:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE   procedure [dbo].[sTraderPrecMetals]
as begin 
  declare @DATE datetime
  declare @PORTID int
  declare @NAV money
  declare @TARGVAL money
  declare @WEEKDAY int 
  declare @tradeid int
  declare @CAD int

  set @CAD = 31972
  set @PORTID = 4
  set @WEEKDAY = 2 --Sunday = 1
  select @DATE = convert(datetime,floor(convert(float,max(time)+1))) from tporttrades where portid = @PORTID
  
  print 'Last Processed: ' + convert(varchar,@DATE) + ' '
  while(@DATE < getdate()) begin
    print 'Checking Date: ' + convert(varchar,@DATE) + '(' + convert(varchar,datepart(weekday,@DATE)) + ')'
    if(@WEEKDAY = datepart(weekday,@DATE))begin
      set @tradeid = (select max(tradeid)+1 from port.dbo.tporttrades where portid = @PORTID)
      set @NAV = port.dbo.fportval(@PORTID, @DATE)
      
      select @TARGVAL = (@NAV/count(*)) from (select secid from port.dbo.tporttradeitems where portid = @PORTID group by secid) a
      
      print 'Processing: ' + convert(varchar,@tradeid)+ ', ' + convert(varchar,@TARGVAL)
      
      insert into tporttrades(portid, tradeid, time)
      select @PORTID,@tradeid,@DATE
      
      insert into tporttradeitems
      select @PORTID as PortID, 
             @tradeid as TradeID,
             ti.ExchID,
             ti.SecID, 
             floor(((d.valclose * -1 * sum(ti.units) + @targval) / d.valclose) * 100) / 100 as Units,
             d.valclose as Price,
             0 as Expenses,
             (floor(((d.valclose * -1 * sum(ti.units) + @targval) / d.valclose) * 100) / 100) * d.valclose as TotCost
      from   port.dbo.tporttrades t
             inner join port.dbo.tporttradeitems ti on ti.portid = t.portid and
                                                       ti.tradeid = t.tradeid
             left join port.dbo.tfindaily d on d.exchid = ti.exchid and
                                               d.secid = ti.secid and 
                                               d.date = (select max(date) from tfindaily where exchid = ti.exchid and secid = ti.secid and date <= @DATE)
      where  t.portid = @PORTID and
             d.secid <> @CAD
      group by ti.exchid,
               ti.secid,
               d.valclose,
               d.date
      having 0 <> floor(((d.valclose * sum(ti.units) + @targval) / d.valclose) * 100) / 100 
      
      -- balance out with cash 
      insert into tporttradeitems
      select @PORTID as PortID, 
             @tradeid as TradeID,
             0,
             @CAD, 
             sum(totcost) * -1 as Units,
             1 as Price,
             0 as Expenses,
             sum(totcost) * -1 as TotCost
      from   port.dbo.tporttradeitems ti
      where  ti.portid = @PORTID and
             ti.tradeid = @TRADEID
      having 0 <> sum(totcost) 
      
    end
    set @DATE = @DATE + 1
  end 

end





GO
/****** Object:  StoredProcedure [dbo].[sUpdateAllPos]    Script Date: 09/20/2007 20:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sUpdateAllPos](@PORTID int, @ASOF datetime)
as begin
  
  declare @time   datetime
  declare @exchid int
  declare @secid  int 
  
  delete 
  from  tPortPos
  where  portID = @PORTID and
         oDate >=@ASOF
  
  update tPortPos
  set    cDate = null
  where  cDate >= @ASOF
  
  declare curTrades cursor for
    select t.time,
           i.exchid,
           i.secid
    from   tPortTrades t
           inner join tPortTradeItems i on t.TradeID = i.TradeID
    where  t.time >= @ASOF
  
  open curTrades
  fetch next from curTrades into @time, @exchid, @secid
  while(@@FETCH_STATUS=0)begin
    exec sUpdatePos @PORTID, @time, @exchid, @secid
  end
  close curTrades
  deallocate curTrades
  
end

GO
/****** Object:  StoredProcedure [dbo].[sUpdatePortHistory]    Script Date: 09/20/2007 20:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sUpdatePortHistory]
AS BEGIN

  declare @DATE datetime
  select @DATE = convert(datetime,floor(convert(float,getdate())))
  
  delete from tporthistory where marktime = @DATE
  
  insert into tporthistory(portid, marktime, nav, units)
  select p.portid,
         @DATE,
         dbo.fportval(p.portid, @DATE),
         dbo.fportunits(p.portid, @DATE)
  from   tportportfolios p
  where  dbo.fportval(p.portid, @DATE) is not null
  
  update tporthistory
  set    unitval = nav/units
  where  unitval is null
  
END

GO
/****** Object:  StoredProcedure [dbo].[sUpdatePos]    Script Date: 09/20/2007 20:16:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sUpdatePos](@PORTID int, @TIME datetime, @EXCHID int, @SECID int)
as begin
  
  declare @isNew    bit
  
  declare @oDate   datetime
  declare @cDate   datetime
  declare @NetPos  money
  declare @NetCost money

  -- update the portfolio history table
  delete 
  from   tporthistory
  where  portid = @PORTID and
         marktime >= @TIME
  
  insert into tporthistory(portid, marktime, nav, units)
  select @PORTID,
         date,
         dbo.fportval(@PORTID, date),
         dbo.fportunits(@PORTID, date)
  from   tfindaily
  where  date >= @TIME
  
  update tporthistory
  set    unitval = nav/units
  where  unitval is null
  
  -- update the positions table  
  select isNew = case 
                   when @oDate is null
                   then 1
                   else 0
                 end
  from   tPortPos
  where  exchid = @EXCHID and
         secid = @SECID and
         oDate >= @TIME and
         cDate is null
  
  select @odate   = min(t.time),
         @cdate   = max(t.time),
         @netpos  = sum(units),
         @netcost = sum(totcost)
  from   tPortTrades t
         inner join tPortTradeItems i on t.tradeid = i.tradeid
  where  i.portid =  @PORTID and 
         i.exchid =  @exchid and 
         i.secid  =  @secid  and
         t.time   <= @time
  
  if(@netpos <> 0)begin
    set @cdate = null
  end --if
  
  if(@isNew = 1)begin
    insert into tPortPos(
      PortID,
      ExchID,
      SecID,
      oDate,
      cDate,
      NetPos,
      NetCost
    )
    values(
      @PORTID,
      @EXCHID,
      @SECID,
      @oDate,
      @cDate,
      @NetPos,
      @NetCost
    )
  end
  else begin
    update tPortPos
    set    cDate = @cDate,
           netpos = @netpos,
           netcost = @netcost
    where  exchid = @EXCHID and
           secid = @SECID and
           oDate >= @oDate
  end
end



