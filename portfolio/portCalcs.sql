﻿create or replace function CalcTotals(portfolio bigint, startdate date)
returns integer as $$
declare
    currdate date;
    lastdate date;
begin
    delete
    from   PortTotals
    where  portid = portfolio and
           histdate = startdate;

    lastdate := (select max(histdate) from history);
    currdate := (select max(histdate)+1 from PortTotals where portid = portfolio);
    
    if(currdate is null)then
        currdate := (select min(transdate) from trans where portid = portfolio);
    end if;

    loop
        select currdate,
               sum(units*price) as total
        from   (
                 select symbol,
                        sum(units) as units,
                        stockprice(symbol,currdate) as price
                 from   trans t
                        inner join transdetails d on t.transid = d.transid
                 where  d.transdate <= currdate
                 group by symbol
               ) a
        group by currdate
        ;
        
        currdate:=currdate + 1;
    exit when (lastdate <= currdate);  -- same result as previous example
END LOOP;

end;
$$ language plpgsql;


create or replace function CalcTotals(portfolio bigint)
returns integer as $$
begin
    perform CalcTotals('1900-01-01');
end;
$$ language plpgsql;

