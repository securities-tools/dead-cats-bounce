﻿select 
       histdate,
       sum(case when symbol = 'CAD' then units*price else 0 end) as "CAD",
       sum(case when symbol = 'XAU' then units*price else 0 end) as "XAU",
       sum(case when symbol = 'XIN' then units*price else 0 end) as "XIN",
       sum(case when symbol = 'XRE' then units*price else 0 end) as "XRE",
       sum(case when symbol = 'XBB' then units*price else 0 end) as "XBB",
       porttotal(histdate) as Total
from (



select distinct
       histdate,
       s.symbol,
       (
         select sum(units) 
         from   trans t 
                inner join transdetail d on t.transid = d.transid and 
                                            t.transdate<=h.histdate and 
                                            d.symbol = s.symbol
       ) units,
       stockprice(s.symbol,histdate) price
from   (select distinct histdate from history) h,
       (select distinct symbol from transdetail) s
where  histdate >= '2006-11-01'
order by histdate, symbol


) a
where units is not null and units <> 0
group by histdate



