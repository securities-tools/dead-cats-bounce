﻿
CREATE TABLE tImportData(
	Ticker varchar(50) NULL,
	Exch char(1) NULL,
	valOpen money NULL,
	valHigh money NULL,
	valLow money NULL,
	valClose money NULL,
	Change money NULL,
	Vol bigint NULL,
	Trades int NULL,
	Date date NULL
);
CREATE TABLE tImportError(
	Exch char(1) NOT NULL,
	Security varchar(15) NOT NULL,
	"Date" date NOT NULL,
	valOpen money NOT NULL,
	valClose money NOT NULL,
	valHigh money NOT NULL,
	valLow money NOT NULL,
	Change money NOT NULL,
	Volume bigint NOT NULL,
	Trades int NOT NULL
);

CREATE TABLE tImportExch(
	ExchID char(1) NOT NULL,
	LocalExchID int NULL,
	CONSTRAINT pk_tImportExch PRIMARY KEY(ExchID)
);

CREATE TABLE tRawDaily(
	Exch char(1) NOT NULL,
	Security varchar(15) NOT NULL,
	Date date NOT NULL,
	valOpen money NOT NULL,
	valClose money NOT NULL,
	valHigh money NOT NULL,
	valLow money NOT NULL,
	Change money NOT NULL,
	Volume bigint NOT NULL,
	Trades int NOT NULL,
	CONSTRAINT PK_tImportDaily PRIMARY KEY (
		Exch,
		Security,
		Date
	)
);

CREATE TABLE tRawPredictions(
	Date date NULL,
	yZScore numeric(21, 6) NULL,
	HDays int NULL,
	FDays int NULL,
	PredZScore money NULL,
	Freq int NULL,
	ZeroPredZScore money NULL,
	ZeroFreq int NULL,
	PosPredZScore money NULL,
	PosFreq int NULL,
	NegPredZScore money NULL,
	NegFreq int NULL
);


CREATE TABLE tRawSecStats(
	Exch char(1) NOT NULL,
	Security varchar(15) NOT NULL,
	Date date NOT NULL,
	HDays int NULL,
	FDays int NULL,
	PrevChange money NULL,
	CurrChange money NULL,
	PrevClose money NULL,
	CurrClose money NULL,
	AvgClose money NULL,
	STDClose money NULL,
	ZClose money NULL,
	AvgChange money NULL,
	STDChange money NULL,
	ZChange money NULL,
	AvgTrades money NULL,
	StdTrades money NULL,
	ZTrades money NULL
);

CREATE TABLE tTmpFX(
	Name varchar(255) NULL,
	Code varchar(4) NULL,
	CAD varchar(15) NULL,
	ToCad varchar(15) NULL
);


CREATE TABLE tTmpRecommend(
	dte date NOT NULL,
	nDte date NULL,
	predVal money NULL,
	curVal money NULL,
	actVal money NULL,
	profit money NULL,
	symb varchar(20) NULL,
	exch varchar(20) NULL
);

CREATE TABLE tTmpStockData(
	Ticker varchar(50) NULL,
	Date varchar(50) NULL,
	Exch varchar(50) NULL,
	valOpen varchar(50) NULL,
	valHigh varchar(50) NULL,
	valLow varchar(50) NULL,
	valClose varchar(50) NULL,
	Change varchar(50) NULL,
	Vol varchar(50) NULL,
	Trades varchar(50) NULL
);
