USE [stockhistory]
GO
/****** Object:  Table [dbo].[tImportData]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tImportData](
	[Ticker] [varchar](50) NULL,
	[Exch] [char](1) NULL,
	[valOpen] [money] NULL,
	[valHigh] [money] NULL,
	[valLow] [money] NULL,
	[valClose] [money] NULL,
	[Change] [money] NULL,
	[Vol] [bigint] NULL,
	[Trades] [int] NULL,
	[Date] [smalldatetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tImportError]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tImportError](
	[Exch] [char](1) NOT NULL,
	[Security] [varchar](15) NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[valOpen] [money] NOT NULL,
	[valClose] [money] NOT NULL,
	[valHigh] [money] NOT NULL,
	[valLow] [money] NOT NULL,
	[Change] [money] NOT NULL,
	[Volume] [bigint] NOT NULL,
	[Trades] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tImportExch]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tImportExch](
	[ExchID] [char](1) NOT NULL,
	[LocalExchID] [tinyint] NULL,
 CONSTRAINT [PK_tImportExch] PRIMARY KEY CLUSTERED 
(
	[ExchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tRawDaily]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tRawDaily](
	[Exch] [char](1) NOT NULL,
	[Security] [varchar](15) NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[valOpen] [money] NOT NULL,
	[valClose] [money] NOT NULL,
	[valHigh] [money] NOT NULL,
	[valLow] [money] NOT NULL,
	[Change] [money] NOT NULL,
	[Volume] [bigint] NOT NULL,
	[Trades] [int] NOT NULL,
 CONSTRAINT [PK_tImportDaily] PRIMARY KEY CLUSTERED 
(
	[Exch] ASC,
	[Security] ASC,
	[Date] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tRawPredictions]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tRawPredictions](
	[Date] [datetime] NULL,
	[yZScore] [numeric](21, 6) NULL,
	[HDays] [int] NULL,
	[FDays] [int] NULL,
	[PredZScore] [money] NULL,
	[Freq] [int] NULL,
	[ZeroPredZScore] [money] NULL,
	[ZeroFreq] [int] NULL,
	[PosPredZScore] [money] NULL,
	[PosFreq] [int] NULL,
	[NegPredZScore] [money] NULL,
	[NegFreq] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tRawSecStats]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tRawSecStats](
	[Exch] [char](1) NOT NULL,
	[Security] [varchar](15) NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[HDays] [int] NULL,
	[FDays] [int] NULL,
	[PrevChange] [money] NULL,
	[CurrChange] [money] NULL,
	[PrevClose] [money] NULL,
	[CurrClose] [money] NULL,
	[AvgClose] [money] NULL,
	[STDClose] [money] NULL,
	[ZClose] [money] NULL,
	[AvgChange] [money] NULL,
	[STDChange] [money] NULL,
	[ZChange] [money] NULL,
	[AvgTrades] [money] NULL,
	[StdTrades] [money] NULL,
	[ZTrades] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTmpFX]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTmpFX](
	[Name] [varchar](255) NULL,
	[Code] [varchar](4) NULL,
	[CAD] [varchar](15) NULL,
	[ToCad] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTmpRecommend]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTmpRecommend](
	[dte] [smalldatetime] NOT NULL,
	[nDte] [datetime] NULL,
	[predVal] [money] NULL,
	[curVal] [money] NULL,
	[actVal] [money] NULL,
	[profit] [money] NULL,
	[symb] [varchar](20) NULL,
	[exch] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTmpStockData]    Script Date: 09/20/2007 20:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTmpStockData](
	[Ticker] [varchar](50) NULL,
	[Date] [varchar](50) NULL,
	[Exch] [varchar](50) NULL,
	[valOpen] [varchar](50) NULL,
	[valHigh] [varchar](50) NULL,
	[valLow] [varchar](50) NULL,
	[valClose] [varchar](50) NULL,
	[Change] [varchar](50) NULL,
	[Vol] [varchar](50) NULL,
	[Trades] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF