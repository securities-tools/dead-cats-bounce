﻿
select zscore, avg(profit) as average, var_pop(profit) as variance--, corr(zscore,profit), covar_pop(zscore,profit)
from (

select trunc((change-historic)/historicdev,2) as zscore, profit
from (

select n.exch, 
       n.security, 
       n.date,
       n.change/(n.valclose-n.change) as change,
       avg(y.change/(y.valclose-y.change)) as historic, 
       stddev(y.change/(y.valclose-y.change)) as historicdev, 
       (max(f.valhigh)-n.valclose)/n.valclose as profit 
from   trawdaily n 
       inner join trawdaily y on n.exch = y.exch and n.security= y.security and 
                                 y.date between n.date-100 and n.date 
       inner join trawdaily f on n.exch = f.exch and n.security= f.security and 
                                 f.date between n.date+1 and n.date+7 
where  not (n.exch in ('F','I','S')) and left(n.security,1) <> '$' and --remove  non-stock groups
       n.valclose <> 0 and (y.valclose-y.change) <> 0 and
       n.date between '2006-01-09' and '2006-01-10'
group by n.exch, 
         n.security,
         n.change, 
         n.date 
having avg(y.volume*y.valclose)>100000 and 
       avg(y.trades)>100 and
       stddev(y.change/(y.valclose-y.change)) > 0
) a

)b
group by zscore
order by variance
;

