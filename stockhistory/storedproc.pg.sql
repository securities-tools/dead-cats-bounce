﻿create or replace function  sCalcDaily(calcdate date = null) RETURNS void AS $$
DECLARE
BEGIN
    if(calcdate is null)then
        select max(date) into calcdate from tRawDaily where date not in (select date from tRawSecStats);
    end if;
    
    --generate some stock data
    for i in 1 .. 1 loop
        perform sCalcSecStats(calcdate, calcdate, 100, i);
    end loop;
    
    --generate predictions for tommorow's prices
    perform sCalcPredictions(calcdate, true);
END $$ LANGUAGE plpgsql;



/******************************************************************************
 * We are attempting to predict *one* day in the future. In this case we
 * should discount the friday -> monday split because there is a lot that can 
 * happen in those 2 days. We are predicting one day change and the events 
 * that can take place in one day (which aren't as much as 2-3 days).
 *****************************************************************************/
create or replace function  sCalcPredictions(calcdate date = null, overwrite boolean = null) returns void as $$
declare
begin
	--raise notice 'Running: Calc Predictions';
	
    if(OVERWRITE is null)then
		overwrite := not (calcdate is null);
    end if;

    if(calcdate is null)then
        select max(date) into calcdate from tRawSecStats where date not in (select date from tRawPredictions where overwrite);
    end if;
  
    if(OVERWRITE)then
        delete 
        from  tRawPredictions 
        where date = calcdate;
    end if;
  
    insert into tRawPredictions
    select calcdate as Date, 
           floor(y.zChange*10.0)/10.0 as yZScore,
           y.HDays              as HDays,
           y.FDays               as FDays,
           avg(t.zChange)             as PredZScore,
           count(*)                   as Freq,
           avg(case when t.zChange=0 then t.zChange else null end) as ZeroPredZScore,
           sum(case when t.zChange=0 then 1 else 0 end)            as ZeroFreq,
           avg(case when t.zChange>0 then t.zChange else null end) as PosPredZScore,
           sum(case when t.zChange>0 then 1 else 0 end)            as PosFreq,
           avg(case when t.zChange<0 then t.zChange else null end) as NegPredZScore,
           sum(case when t.zChange<0 then 1 else 0 end)            as NegFreq
    from   tRawSecStats t
           inner join tRawSecStats y on t.date between (calcdate-300) and calcdate and
                                        (y.date+t.fdays) = t.date and
                                        y.exch = t.exch and
                                        y.security = t.security and 
                                        y.hdays = t.hdays and
                                        y.fdays = t.fdays 
    where  y.zchange is not null and
           t.zchange is not null 
    group by floor(y.zChange*10.0)/10.0,
           y.HDays,
           y.FDays
    ;
    
    delete 
    from  tRawPredictions
    where PredZScore is null
    ;
  
end $$ language plpgsql;


/*** sCalcSecStats *************************************************************
*   Desc: Generates statistics on a given stock based on historical data,
*         and places them in the table StockStats for future processing and
*         analysis. This data compares past values to future values to test
*         for a meaningful and predictable correlation between the two.
*         
*         THIS IS THE ONLY THING THAT SHOULD EVER WRITE TO STOCKSTATS.
*         
*         The Stockstats table was designed because this query was taking up
*         to an hour to run for all data. Since results never change and it
*         only takes aproximately ten minutes to calculate a days worth of 
*         data, I decided to store the results in a table. Any changes to 
*         this query must be reflected in the table, and the other way.
* 
*         Also note that the UPDATE was added because there is enough data to
*         precalculate zScore and I am starting to use that more. Rather than
*         call AVG and STDDEV a couple of times (once for the 
*         avgChange/stdChange and once for the zChange), I thought it would
*         be better to just do a calculation after the fact using the
*         precalculated avg and zscore.
*
*  Input: SDATE - date - the first day in the range to perform
*                             calculations on.
*         EDATE - date - the last day in the range to perform
*                             calculations on.
*         HDAYS - int      - number of days to use as historical data in
*                             statistical calculations.
*         FDAYS - int      - number of days to look forward for the
*                             results of the historical data.
*
* Output: None.
* 
*   ToDo: ( ) Include HDAYS and FDAYS in query.
*             Currently that is hard coded into the system.
*  
*******************************************************************************/
create or replace function  sCalcSecStats(
  SDATE date = null,  --start date
  EDATE date = null,  --end date
  HistDAYS int = null,       --historical days
  FutrDAYS int = null        --future days
) returns void AS $$
BEGIN
    if(SDATE is null)then
        SDATE := current_date;
    end if;
    SDATE := date_trunc('day',SDATE);
  
    if(EDATE is null)then
        EDATE := current_date;
    end if;
    EDATE := date_trunc('day',EDATE);

    if(HistDAYS is null)then
        HistDAYS := 100;
    end if;

    if(FutrDAYS is null)then
        FutrDAYS := 3;
    end if;

    delete 
    from   tRawSecStats s
    where  s.date between SDATE and EDATE and
           s.hdays = HistDAYS and 
           s.fdays = FutrDAYS
    ;

    insert into tRawSecStats
    select 
         t.Exch,
         t.Security,
         t.Date,
         HistDAYS as HDays,
         FutrDAYS as FDays,
         (100.0*t.change/(t.valclose-t.change)) as CurrChange,
         t.valclose as CurrClose,
         avg(h.valclose) as AvgClose,
         avg(h.valclose) as STDevClose,
         null as ZClose,
         avg(100.0*h.change/(h.valclose-h.change)) as AvgChange,
         stddev(100.0*h.change/(h.valclose-h.change)) as STDevChange,
         null as ZChange,
         avg(h.trades * 1.0) as AvgTrades,
         stddev(h.trades * 1.0) as StdTrades,
         null as ZTrades
    from   tRawDaily t   --today
         inner join tRawDaily f on t.exch = f.exch and   --yesterday
                                   t.security = f.security and 
                                   f.date between t.date+1 and (t.date+FutrDAYS) and 
                                   f.valclose > 0
         inner join tRawDaily h on t.exch = h.exch and    --historical (past 60 days)
                                   t.security = h.security and
                                   h.date between (t.date-HistDAYS) and t.date-1 and 
                                   h.valclose > 0
    where  t.date between SDATE and EDATE and --restrict to date range
         t.valclose > 5 and 
         not (t.exch in ('F','I','S')) and  --remove  non-stock groups
         left(t.security,1) <> '$' and  --remove  non-stock groups
         t.valclose-t.change <> 0 and h.valclose-h.change <> 0 and f.valclose-f.change <> 0
    group by t.exch,
           t.security,
           t.date,
           t.valclose
    having avg(h.valclose*h.volume) > 10000
           ;
	
    update tRawSecStats
    set    zChange = (coalesce(currchange,0)-coalesce(avgchange,0))/stdchange,
           zClose = (coalesce(currclose,0)-coalesce(avgclose,0))/stdclose
    where  (zChange is null or zClose is null)  and
           coalesce(stdchange,0) <> 0 and
           coalesce(stdclose,0) <> 0 and
           date between SDATE and EDATE;

END $$ language plpgsql;


/*******************************************************************************
*
*******************************************************************************/
create or replace function  sImport() returns void AS $$ 
declare
BEGIN
    -- make sure the change column is calculated
    update timportdata
    set    change = valclose-coalesce((select d.valclose from trawdaily d where d.exch = timportdata.exch and d.security = timportdata.ticker order by date desc limit 1),valclose)
    where  change is null;
    
    -- make sure the change column is calculated
    update timportdata
    set    vol = 0
    where  vol is null;
    
    -- make sure the change column is calculated
    update timportdata
    set    trades = 0
    where  trades is null;
    
    -- insert the data
    insert into trawdaily(exch, security, date, valopen, valclose, valhigh, vallow, change, volume, trades)
    select i.exch,
           i.ticker,
           i.date,
           i.valopen,
           i.valclose,
           i.valhigh,
           i.vallow,
           i.change,
           i.vol,
           i.trades
    from   timportdata i
           left join trawdaily d on i.exch = d.exch and 
                                    i.ticker = d.security and 
                                    i.date = d.date
    where  d.exch is null
    ;
    
    -- clean out whatever is left
    truncate table timportdata;

END $$ language plpgsql;



create or replace function  sTmpRefreshSecStats() returns void as $$ 
declare 
    calcdate date;
begin
    select min(date) into calcdate from trawsecstats where not(zclose is null);
    
    while (calcdate > '2005-01-02') loop
		raise notice 'Processing: %', calcdate;
        perform sCalcDaily(calcdate);
        calcdate := calcdate - 1;
        --perform pg_sleep(1);
    end loop;
end $$ language plpgsql;


