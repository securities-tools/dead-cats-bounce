USE [stockhistory]
GO
/****** Object:  StoredProcedure [dbo].[sCalcDaily]    Script Date: 09/20/2007 20:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[sCalcDaily](
  @DATE as datetime = null
)
AS BEGIN
  
  if(@DATE is null)begin
    select @DATE = max(date) from tRawDaily
  end

  --generate some stock data
  exec dbo.sCalcSecStats @DATE, @DATE, 90, 1 --Jeff Events
  exec dbo.sCalcSecStats @DATE, @DATE, 90, 60 --Terry Events
  
  --generate predictions for tommorow's prices
  exec dbo.sCalcPredictions @DATE, 0
  
END



GO
/****** Object:  StoredProcedure [dbo].[sCalcPredictions]    Script Date: 09/20/2007 20:20:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
 * We are attempting to predict *one* day in the future. In this case we
 * should discount the friday -> monday split because there is a lot that can 
 * happen in those 2 days. We are predicting one day change and the events 
 * that can take place in one day (which aren't as much as 2-3 days).
 *****************************************************************************/
CREATE  procedure [dbo].[sCalcPredictions](@DATE datetime = null, @OVERWRITE bit = 1) as
begin

  if(@DATE is null)begin
    select @DATE = max(date)from tRawSecStats
  end
  
  if(@OVERWRITE is null)begin
    set @OVERWRITE = 1
  end

  if(@OVERWRITE = 1)begin
    delete 
    from  tRawPredictions 
    where date = @DATE
  end 

  insert into tRawPredictions
  select @DATE                      as Date, 
         floor(y.zChange*10.0)/10.0 as yZScore,
         y.HDays              as HDays,
         y.FDays               as FDays,
         avg(t.zChange)             as PredZScore,
         count(*)                   as Freq,
         avg(case when t.zChange=0 then t.zChange else null end) as ZeroPredZScore,
         sum(case when t.zChange=0 then 1 else 0 end)            as ZeroFreq,
         avg(case when t.zChange>0 then t.zChange else null end) as PosPredZScore,
         sum(case when t.zChange>0 then 1 else 0 end)            as PosFreq,
         avg(case when t.zChange<0 then t.zChange else null end) as NegPredZScore,
         sum(case when t.zChange<0 then 1 else 0 end)            as NegFreq
  from   tRawSecStats t
         inner join tRawSecStats y on t.date between (@DATE-300) and @DATE and
                                    (y.date+t.fdays) = t.date and
                                    y.exch = t.exch and
                                    y.security = t.security and 
                                    y.hdays = t.hdays and
                                    y.fdays = t.fdays 
  where y.zchange is not null and
        t.zchange is not null 
  group by floor(y.zChange*10.0)/10.0,
           y.HDays,
           y.FDays
  
  delete 
  from tRawPredictions
  where PredZScore is null
  
end




GO
/****** Object:  StoredProcedure [dbo].[sCalcSecStats]    Script Date: 09/20/2007 20:20:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*** sCalcSecStats *************************************************************
*   Desc: Generates statistics on a given stock based on historical data,
*         and places them in the table StockStats for future processing and
*         analysis. This data compares past values to future values to test
*         for a meaningful and predictable correlation between the two.
*         
*         THIS IS THE ONLY THING THAT SHOULD EVER WRITE TO STOCKSTATS.
*         
*         The Stockstats table was designed because this query was taking up
*         to an hour to run for all data. Since results never change and it
*         only takes aproximately ten minutes to calculate a days worth of 
*         data, I decided to store the results in a table. Any changes to 
*         this query must be reflected in the table, and the other way.
* 
*         Also note that the UPDATE was added because there is enough data to
*         precalculate zScore and I am starting to use that more. Rather than
*         call AVG and STDDEV a couple of times (once for the 
*         avgChange/stdChange and once for the zChange), I thought it would
*         be better to just do a calculation after the fact using the
*         precalculated avg and zscore.
*
*  Input: @SDATE - datetime - the first day in the range to perform
*                             calculations on.
*         @EDATE - datetime - the last day in the range to perform
*                             calculations on.
*         @HDAYS - int      - number of days to use as historical data in
*                             statistical calculations.
*         @FDAYS - int      - number of days to look forward for the
*                             results of the historical data.
*
* Output: None.
* 
*   ToDo: ( ) Include @HDAYS and @FDAYS in query.
*             Currently that is hard coded into the system.
*  
*******************************************************************************/
create       PROCEDURE [dbo].[sCalcSecStats](
  @SDATE as datetime = null,  --start date
  @EDATE as datetime = null,  --end date
  @HDAYS as int = null,       --historical days
  @FDAYS as int = null        --future days
)
AS BEGIN
  if(@SDATE is null)begin
    set @SDATE = getdate()
  end
  set @SDATE = convert(datetime,floor(convert(float,@SDATE)))
  
  if(@EDATE is null)begin
    set @EDATE = getdate()
  end
  set @EDATE = convert(datetime,floor(convert(float,@EDATE)))
  
  if(@HDAYS is null)begin
    set @HDAYS = 3
  end 
  
  if(@FDAYS is null)begin
    set @FDAYS = 3
  end 
  
  delete 
  from   tRawSecStats 
  where  date between @SDATE and @EDATE and
         hdays = @HDAYS and 
         fdays = @FDAYS;

  insert into tRawSecStats
  select 
         t.Exch,
         t.Security,
         t.Date,
         @HDAYS as HDays,
         @FDAYS as FDays,
         convert(money,100.0*y.change/yp.valclose) as PrevChange,
         convert(money,100.0*t.change/y.valclose) as CurrChange,
         convert(money,y.valclose) as PrevClose,
         convert(money,t.valclose) as CurrClose,
         convert(money,avg(h.valclose)) as AvgClose,
         convert(money,avg(h.valclose)) as STDevClose,
         convert(money,null) as ZClose,
         convert(money,avg(100.0*h.change/hp.valClose)) as AvgChange,
         convert(money,stdev(100.0*h.change/hp.valClose)) as STDevChange,
         convert(money,null) as ZChange,
         convert(money,avg(h.trades * 1.0)) as AvgTrades,
         convert(money,stdev(h.trades * 1.0)) as StdTrades,
         convert(money,null) as ZTrades
  from   tRawDaily t   --today
         inner join tRawDaily y on t.exch = y.exch and   --yesterday
                                   t.security = y.security and 
                                   y.valclose > 0
         inner join tRawDaily yp on yp.exch = y.exch and   --yesterday
                                    yp.security = y.security and
                                    yp.valclose > 0
         inner join tRawDaily h on t.exch = h.exch and    --historical (past 60 days)
                                   t.security = h.security and
                                   h.date between (y.date-@HDAYS-1) and y.date-1 and 
                                   h.valclose > 0
         inner join tRawDaily hp on h.exch = hp.exch and    --historical (past 60 days)
                                    h.security = hp.security and 
                                    hp.valclose > 0
  where  t.date between @SDATE and @EDATE and --restrict to date range
         t.valclose > 5 and 
         not (t.exch in ('F','I','S')) and  --remove  non-stock groups
         left(t.security,1) <> '$' and  --remove  non-stock groups
         y.date = (select max(a.date)
                   from   tRawDaily a
                   where  a.exch     =  t.exch and
                          a.security =  t.security and
                          a.date     <= t.date-@FDAYS   
                  ) and
         yp.date = (select max(c.date)
                    from   tRawDaily c
                    where  c.exch     = y.exch and
                           c.security = y.security and
                           c.date     < y.date) and
         hp.date = (select max(b.date) 
                    from   tRawDaily b
                    where  b.exch = h.exch and
                           b.security = h.security and
                           b.date < h.date) 
  group by t.exch,
           t.security,
           t.date,
           convert(money,100.0*y.change/yp.valclose),
           convert(money,100.0*t.change/y.valclose),
           convert(money,y.valclose),
           convert(money,t.valclose)

  update tRawSecStats
  set    zChange = (isnull(currchange,0)-isnull(avgchange,0))/stdchange,
         zClose = (isnull(currclose,0)-isnull(avgclose,0))/stdclose
  where  (zChange is null or zClose is null)  and
         isnull(stdchange,0) <> 0 and
         date between @SDATE and @EDATE;

END
/*** sCalcSecStats ************************************************************/










GO
/****** Object:  StoredProcedure [dbo].[sImport]    Script Date: 09/20/2007 20:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sImport]
AS BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

-- make sure the change column is calculated
update timportdata
set    change = valclose-isnull((select top 1 d.valclose from trawdaily d where d.exch = timportdata.exch and d.security = timportdata.ticker order by date desc),valclose)
where  change is null

-- make sure the change column is calculated
update timportdata
set    vol = 0
where  vol is null

-- make sure the change column is calculated
update timportdata
set    trades = 0
where  trades is null

-- insert the data
insert into trawdaily(exch, security, date, valopen, valclose, valhigh, vallow, change, volume, trades)
select i.exch,
       i.ticker,
       i.date,
       i.valopen,
       i.valclose,
       i.valhigh,
       i.vallow,
       i.change,
       i.vol,
       i.trades
from   timportdata i
       left join trawdaily d on i.exch = d.exch and 
                                i.ticker = d.security and 
                                i.date = d.date
where  d.exch is null

-- clean out whatever is left
truncate table timportdata

END


GO
/****** Object:  StoredProcedure [dbo].[sTmpRefreshSecStats]    Script Date: 09/20/2007 20:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sTmpRefreshSecStats]
as begin

declare @DATE datetime

select @DATE = min(date) from trawsecstats where not(zclose is null)

while (@DATE > '2005-01-02') begin
  print 'Processing: ' + convert(varchar,@DATE)
  exec dbo.sCalcDaily @DATE
  set @DATE = @DATE - 1
  waitfor delay '00:00:10'
end 

end
